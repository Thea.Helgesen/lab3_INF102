package INF102.lab3.peakElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeakRecursive implements IPeak {

    Map<List<Integer>, Integer> memory = new HashMap<>();

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            return 0;
        }
        if (memory.containsKey(numbers)) {
            return memory.get(numbers);
        }

        int peak = findPeak(numbers, 0, numbers.size()-1);
        memory.put(numbers, peak);

        return peak;
    }
                
    private int findPeak(List<Integer> numbers, int left, int right) {
        if (left == right) {
            return numbers.get(left);
        }

        int mid = left + (right - left) / 2;

        if (mid == 0 && numbers.get(mid) >= numbers.get(mid + 1)) {
            return numbers.get(mid);
        } else if (mid == numbers.size()-1 && numbers.get(mid) >= numbers.get(mid - 1)) {
            return numbers.get(mid);
        } else if (numbers.get(mid) >= numbers.get(mid + 1) && numbers.get(mid) >= numbers.get(mid - 1)) {
            return numbers.get(mid);
        }

        else if (numbers.get(mid) < numbers.get(mid + 1)) {
            return findPeak(numbers, mid + 1, right); 
        } else {
            return findPeak(numbers, left, mid - 1);
        }
    } 
}

