package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        // Check base case
        if (list.isEmpty()) {
            return 0;   
        } else {
            long first = list.get(0);
            List<Long> rest = list.subList(1, list.size());
            return first + sum(rest);
        }
    }
}
