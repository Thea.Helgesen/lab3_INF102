package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    int guessCounter;

	@Override
    public int findNumber(RandomNumber number) {
        int low = number.getLowerbound();
        int high = number.getUpperbound();
        guessCounter = 0;
        return guessNumber(number, low, high);
    }

    private int guessNumber(RandomNumber number, int low, int high) {
        int mid = (low + high) / 2;
        int guessed = number.guess(mid);
        guessCounter++;

        if (guessed == 0) {
            return mid;
        } else if (guessed == -1) {
            return guessNumber(number, mid + 1, high);
        } else {
            return guessNumber(number, low, mid - 1);
        }
    }
}
